// let's use a constant here so all we have to do is change this
// and of course the function name, but I left that alone
const TOP_MAX = 100;

let ipCountsMap = new Map();
let topResultsMap = new Map();

// public
function requestHandled(ipAddress) {
  // default count is 1
  let count = 1;

  // check is ip has been counted before...
  const current = ipCountsMap.get(ipAddress);
  if (current) {
    // ... if so, increment count
    count = current + 1;
  }

  // update dataset
  ipCountsMap.set(ipAddress, count);
  // update sub-dataset
  insertTopResult(ipAddress, count);
}

// public
function top100() {
  // sort in place so we get benefits on insert
  sortTopResults();
  // map the map to an array of objects for easier handling in implementation
  return [...topResultsMap].map(([ipAddress, count]) => ({ ipAddress, count }));
}

// public
function clear() {
  ipCountsMap = new Map();
  topResultsMap = new Map();
}

// private
function $getRequests() {
  return ipCountsMap;
}

// internal
function sortTopResults() {
  const results = [...topResultsMap];
  // sort by count in descending order
  results.sort(([ipA, countA], [ipB, countB]) => countB - countA);
  topResultsMap = new Map(results);
}

// internal
function insertTopResult(ipAddress, count) {
  // if ip is already in top 100, update count
  if (topResultsMap.has(ipAddress)) {
    topResultsMap.set(ipAddress, count);
    return true;
  }

  // we don't have 100 ips yet, so it default in the top 100
  if (topResultsMap.size < TOP_MAX) {
    topResultsMap.set(ipAddress, count);
    return true;
  }

  // if we get here we need to compare new with current top100
  const entries = topResultsMap.entries();
  // iterate in reverse because there is chance this is sorted for us
  for (let x = entries.length - 1; x > -1; x--) {
    const [cursorIp, cursorValue] = entries[x];
    // if insert candidate is greater than or EQUAL to existing value, swap and we're done
    // I chose to swap equal values because its faster and best newer data it probably better
    if (count >= cursorValue) {
      topResultsMap.delete(cursorIp);
      topResultsMap.set(ipAddress, count);
      return true;
    }
  }

  return false;
}

module.exports = {
  requestHandled,
  top100,
  clear,
  $getRequests,
};
