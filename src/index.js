// publicly exported functions for use externally somewhere

const { requestHandled, top100, clear } = require("./ip-logger");

module.exports = {
  requestHandled,
  top100,
  clear,
};
