const { requestHandled, top100, clear, $getRequests } = require("../ip-logger");

describe("IP Logger", () => {
  beforeEach(() => {
    // clear out requestCounts before each test
    clear();
  });

  describe("requestHandled", () => {
    it("adds IP to requestCounts", () => {
      requestHandled("1.1.1.1");

      const requestCounts = $getRequests();

      expect(requestCounts.get("1.1.1.1")).toBe(1);
    });

    it("increments count when IP requested more than once", () => {
      requestHandled("1.1.1.1");
      requestHandled("1.1.1.1");

      const requestCounts = $getRequests();

      expect(requestCounts.get("1.1.1.1")).toBe(2);
    });

    it("keeps IP counts separate", () => {
      requestHandled("1.1.1.1");
      requestHandled("1.1.1.1");
      requestHandled("1.2.3.4");

      const requestCounts = $getRequests();

      expect(requestCounts.get("1.1.1.1")).toBe(2);
      expect(requestCounts.get("1.2.3.4")).toBe(1);
    });
  });

  describe("top100", () => {
    it("adds value to top100", () => {
      requestHandled("1.1.1.1");

      const topResults = top100();

      expect(topResults[0]).toEqual({ ipAddress: "1.1.1.1", count: 1 });
    });

    it("returns top 100 results sorted highest to lowest", () => {
      // generate some results
      for (let i = 1; i < 10001; i++) {
        // reverse this to make sure data is being sorted by functions being tested
        for (let j = 100; j > 0; j--) {
          requestHandled(i % j);
        }
      }

      const topResults = top100();

      expect(topResults.length).toBe(100);

      // we're not going to check every single one, just a few
      expect(topResults[0]).toEqual({ count: 51834, ipAddress: 0 });
      expect(topResults[42]).toEqual({ count: 8599, ipAddress: 42 });
      expect(topResults[84]).toEqual({ count: 1728, ipAddress: 84 });
      expect(topResults[99]).toEqual({ count: 100, ipAddress: 99 });
    });

    it("does not return more than 100 results", () => {
      for (let i = 1; i < 200; i++) {
        requestHandled(i);
      }

      const topResults = top100();

      expect(topResults.length).toBe(100);
    });
  });

  describe("clear", () => {
    it("clears requestCounts", () => {
      requestHandled("1.1.1.1");

      clear();

      expect($getRequests().size).toBe(0);
    });

    it("clears top100", () => {
      requestHandled("1.1.1.1");

      clear();

      expect(top100().length).toBe(0);
    });

    it("it does not explode when already empty", () => {
      clear();

      expect($getRequests().size).toBe(0);
      expect(top100().length).toBe(0);
    });
  });
});
