#!/usr/bin/env node

const fs = require("fs");
const readline = require("readline");

const { requestHandled, top100, clear } = require(".");

async function processFile(file) {
  const rl = readline.createInterface({
    input: fs.createReadStream(file),
    crlfDelay: Infinity,
  });

  for await (const line of rl) {
    requestHandled(line);
  }

  const topResults = top100();
  process.stdout.write("Results:\n");
  process.stdout.write("================\n");
  topResults.forEach(({ ipAddress, count }, i) => {
    process.stdout.write(`${i + 1}. ${ipAddress} - ${count}\n`);
  });

  clear();
}

processFile(process.argv[2]);
