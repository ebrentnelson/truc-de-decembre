const faker = require("faker");

const { requestHandled, top100, clear } = require(".");

// Simulation using faker.js IP addresses
for (let i = 0; i < 10000000; i++) {
  requestHandled(faker.internet.ip());
}

const topResults = top100();
process.stdout.write("Results:\n");
process.stdout.write("================\n");
topResults.forEach(({ ipAddress, count }, i) => {
  process.stdout.write(`${i + 1}. ${ipAddress} - ${count}\n`);
});

clear();
