# truc-de-decembre

## Prerequisites
You need node.js to be installed to run this bad boy. Might I suggest
https://github.com/nvm-sh/nvm if you do not already have it.

## Getting Started
Before any of the following examples will work we need to install our
dependencies (only used for tests below). See package.json for list of
dependencies.

```
npm install
```

## Running Unit Tests
Running the following will make sure the code is still functioning as expected.

```
npm run test
```

## Running faker.js Simulation
Running the following command will run a simulation of 10 million IP addresses.
Unfortunately faker.js does a really good job at randomization so there are not
a lot of IP collisions so you'll likely always just see counts of 1. It is
still fun to see various results.

This takes ~30s (likely due to faker.js generation time, see below):

```
npm run faker-sim
```

## Running With File Input
Running the following commands will run simulations with predetermined IP
addresses from data files. Due to size constraints I only created files with
1,000, 100,000, and 1,000,000 IP addresses.

Takes ~70ms:

```
./src/file-sim.js input1000.txt
```

Takes ~193ms:

```
./src/file-sim.js input100000.txt
```

Takes ~1s:

```
./src/file-sim.js input1000000.txt
```
